import ta_ip_fabric_declare

import os
import sys
import time
import datetime
import json

import modinput_wrapper.base_modinput
from splunklib import modularinput as smi



import input_module_ip_fabric as input_module

bin_dir = os.path.basename(__file__)

'''
    Do not edit this file!!!
    This file is generated by Add-on builder automatically.
    Add your modular input logic to file input_module_ip_fabric.py
'''
class ModInputip_fabric(modinput_wrapper.base_modinput.BaseModInput):

    def __init__(self):
        if 'use_single_instance_mode' in dir(input_module):
            use_single_instance = input_module.use_single_instance_mode()
        else:
            use_single_instance = False
        super(ModInputip_fabric, self).__init__("ta_ip_fabric", "ip_fabric", use_single_instance)
        self.global_checkbox_fields = None

    def get_scheme(self):
        """overloaded splunklib modularinput method"""
        scheme = super(ModInputip_fabric, self).get_scheme()
        scheme.title = ("IP FABRIC")
        scheme.description = ("Go to the add-on\'s configuration UI and configure modular inputs under the Inputs menu.")
        scheme.use_external_validation = True
        scheme.streaming_mode_xml = True

        scheme.add_argument(smi.Argument("name", title="Name",
                                         description="",
                                         required_on_create=True))

        """
        For customized inputs, hard code the arguments here to hide argument detail from users.
        For other input types, arguments should be get from input_module. Defining new input types could be easier.
        """
        scheme.add_argument(smi.Argument("ip_fabric_url", title="IP Fabric URL",
                                         description="URL to access IP Fabric. Must start with https",
                                         required_on_create=True,
                                         required_on_edit=False))
        scheme.add_argument(smi.Argument("snapshot_id", title="Snapshot ID",
                                         description="UUID of snapshotExample: 12dd8c61-129c-431a-b98b-4c9211571f89",
                                         required_on_create=False,
                                         required_on_edit=False))
        scheme.add_argument(smi.Argument("use_snapshot_timestamp", title="Use IPF snapshot time-stamp",
                                         description="Check if you would like to use IP Fabric\'s snapshot time as event time for data ingestion.",
                                         required_on_create=False,
                                         required_on_edit=False))
        scheme.add_argument(smi.Argument("intent_checks", title="Load Intent Checks",
                                         description="Check if you would like to load all IP Fabric intent checks",
                                         required_on_create=False,
                                         required_on_edit=False))
        scheme.add_argument(smi.Argument("only_report_count_of_events", title="Only report count of events",
                                         description="Check if you only want to report the number of occurrences or rows in a IP Fabric Table",
                                         required_on_create=False,
                                         required_on_edit=False))
        scheme.add_argument(smi.Argument("table_path", title="Table Path",
                                         description="Table path from IP Fabric. Can be API based URL as well \"tables/management/snmp/communities\"",
                                         required_on_create=False,
                                         required_on_edit=False))
        scheme.add_argument(smi.Argument("table_filter", title="Table filter",
                                         description="Filter to apply to table",
                                         required_on_create=False,
                                         required_on_edit=False))
        return scheme

    def get_app_name(self):
        return "TA-ip_fabric"

    def validate_input(self, definition):
        """validate the input stanza"""
        input_module.validate_input(self, definition)

    def collect_events(self, ew):
        """write out the events"""
        input_module.collect_events(self, ew)

    def get_account_fields(self):
        account_fields = []
        return account_fields

    def get_checkbox_fields(self):
        checkbox_fields = []
        checkbox_fields.append("use_snapshot_timestamp")
        checkbox_fields.append("intent_checks")
        checkbox_fields.append("only_report_count_of_events")
        return checkbox_fields

    def get_global_checkbox_fields(self):
        if self.global_checkbox_fields is None:
            checkbox_name_file = os.path.join(bin_dir, 'global_checkbox_param.json')
            try:
                if os.path.isfile(checkbox_name_file):
                    with open(checkbox_name_file, 'r') as fp:
                        self.global_checkbox_fields = json.load(fp)
                else:
                    self.global_checkbox_fields = []
            except Exception as e:
                self.log_error('Get exception when loading global checkbox parameter names. ' + str(e))
                self.global_checkbox_fields = []
        return self.global_checkbox_fields

if __name__ == "__main__":
    exitcode = ModInputip_fabric().run(sys.argv)
    sys.exit(exitcode)
