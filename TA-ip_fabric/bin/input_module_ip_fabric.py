# encoding = utf-8

import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", "lib"))
import time
import datetime
from mini_ipfabric import IPFClient
import json

'''
    IMPORTANT
    Edit only the validate_input and collect_events functions.
    Do not edit any other part in this file.
    This file is generated only once when creating the modular input.
'''
'''
# For advanced users, if you want to create single instance mod input, uncomment this method.
def use_single_instance_mode():
    return True
'''


def add_snapshot_data_to_event(event, snapshot_id, use_snapshot_timestamp, snapshot_time_ms, custom_strftime):
    event["snapshot_id"] = snapshot_id
    if use_snapshot_timestamp:
        event["snapshot_time_ms"] = snapshot_time_ms
        event["snapshot_time"] = datetime.datetime.utcfromtimestamp(snapshot_time_ms / 1000.0).strftime(custom_strftime)
        event["snapshot_time_iso"] = datetime.datetime.utcfromtimestamp(snapshot_time_ms / 1000.0).isoformat()
    return event


def validate_input(helper, definition):
    """Implement your own validation logic to validate the input stanza configurations"""
    # This example accesses the modular input variable
    ip_fabric_url = definition.parameters.get('ip_fabric_url', "")
    if not ip_fabric_url.startswith('https://'):
        raise ValueError('IP Fabric URL must start with https://')


def collect_events(helper, ew):
    table_for_events = None
    ipf = None

    # get global settings
    ip_fabric_url = helper.get_arg("ip_fabric_url")
    global_ip_fabric_token = helper.get_global_setting("ip_fabric_token")
    ip_fabric_ss_id = helper.get_arg("snapshot_id")
    ip_fabric_timout = int(helper.get_global_setting("ip_fabric_client_timeout"))
    global_ip_fabric_path_to_ssl = helper.get_global_setting("system_path_to_cert")
    use_snapshot_timestamp = helper.get_arg("use_snapshot_timestamp")
    only_report_count_of_events = helper.get_arg("only_report_count_of_events")
    custom_strftime = helper.get_global_setting("custom_strftime")

    helper.log_debug(f' SnapShot ID: {ip_fabric_ss_id}')

    if global_ip_fabric_path_to_ssl:
        import ssl
        context = ssl.create_default_context()
        helper.log_debug(f"global_ip_fabric_path_to_ssl: {global_ip_fabric_path_to_ssl}")
        context.load_verify_locations(global_ip_fabric_path_to_ssl)
        ipf = IPFClient(base_url=ip_fabric_url, auth=global_ip_fabric_token, verify=context,
                        snapshot_id=ip_fabric_ss_id, timeout=ip_fabric_timout)

    if not global_ip_fabric_path_to_ssl:
        ipf = IPFClient(base_url=ip_fabric_url, auth=global_ip_fabric_token, verify=True, snapshot_id=ip_fabric_ss_id,
                        timeout=ip_fabric_timout)

    if not ipf:
        helper.log_error(
            "unable to initialize client."
            "please verify all settings")
        return

    ipf.headers['user-agent'] += '; python-splunk-ta'
    snapshots = ipf.get_loaded_snapshots()
    snapshot = snapshots.get(ipf.snapshot_id)
    snapshot_timestamp = snapshot.get("tsEnd")
    event_time = snapshot_timestamp if use_snapshot_timestamp else str(datetime.datetime.utcnow())

    helper.log_info(f"default: sourcetype: {helper.get_sourcetype()}")

    # first check if intent checks need to be collected
    if helper.get_arg("intent_checks"):
        helper.log_debug(f'Intent checks are enabled')
        for intent in ipf.get_intents():
            intent = add_snapshot_data_to_event(intent, ipf.snapshot_id, use_snapshot_timestamp, snapshot_timestamp, custom_strftime)
            event = helper.new_event(
                json.dumps(intent),
                time=event_time,
                host=None,
                index=helper.get_output_index(),
                source=helper.get_input_type(),
                sourcetype=helper.get_sourcetype(),
                done=True,
                unbroken=True)
            ew.write_event(event)
        return

    table_selection_string = helper.get_arg('table_path')

    events = ipf.fetch_all(table_selection_string) \
        if not helper.get_arg("table_filter") \
        else ipf.fetch_all(table_selection_string, filters=json.loads(helper.get_arg("table_filter")))

    helper.log_debug(f'len of events: {len(events)}')

    if only_report_count_of_events:
        json_data = {
            "table_path": table_selection_string,
            "count": len(events),
            "snapshot_id": ipf.snapshot_id
        }
        json_data = add_snapshot_data_to_event(json_data, ipf.snapshot_id, use_snapshot_timestamp, snapshot_timestamp, custom_strftime)
        event = helper.new_event(
            json.dumps(json_data),
            time=event_time,
            host=None,
            index=helper.get_output_index(),
            source=helper.get_input_type(),
            sourcetype=helper.get_sourcetype(),
            done=True,
            unbroken=True)
        ew.write_event(event)
        return

    for data in events:
        data = add_snapshot_data_to_event(data, ipf.snapshot_id, use_snapshot_timestamp, snapshot_timestamp, custom_strftime)
        event = helper.new_event(
            json.dumps(data),
            time=event_time,
            host=None,
            index=helper.get_output_index(),
            source=helper.get_input_type(),
            sourcetype=helper.get_sourcetype(),
            done=True,
            unbroken=True)
        ew.write_event(event)
