[ip_fabric://<name>]
ip_fabric_url = URL to access IP Fabric. Must start with https
snapshot_id = UUID of snapshotExample: 12dd8c61-129c-431a-b98b-4c9211571f89
use_snapshot_timestamp = Check if you would like to use IP Fabric's snapshot time as event time for data ingestion.
intent_checks = Check if you would like to load all IP Fabric intent checks
only_report_count_of_events = Check if you only want to report the number of occurrences or rows in a IP Fabric Table
table_path = Table path from IP Fabric. Can be API based URL as well "tables/management/snmp/communities"
table_filter = Filter to apply to table