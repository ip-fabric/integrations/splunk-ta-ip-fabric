# The official Splunk Add on for IP Fabric

## IP Fabric

[IP Fabric](https://ipfabric.io) is a vendor-neutral network assurance platform that automates the 
holistic discovery, verification, visualization, and documentation of 
large-scale enterprise networks, reducing the associated costs and required 
resources whilst improving security and efficiency.

It supports your engineering and operations teams, underpinning migration and 
transformation projects. IP Fabric will revolutionize how you approach network 
visibility and assurance, security assurance, automation, multi-cloud 
networking, and trouble resolution.

**Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

## Market Place Link
https://classic.splunkbase.splunk.com/app/6707/

### Description

The IP Fabric Splunk TA offers a potent solution for comprehensive network monitoring and analysis.
By utilizing the official IP Fabric app, organizations can seamlessly merge IP Fabric's advanced network visibility 
capabilities with Splunk's robust data collection and analysis functionalities. This collaboration empowers teams to 
swiftly identify and troubleshoot network issues, proactively enhance security measures, and gain valuable insights 
from the amalgamation of IP Fabric's network data and Splunk's powerful analytics platform.

### Please Refer to our Docs for more information!
https://docs.ipfabric.io/latest/integrations/splunk/

[Click here! SneakPeak into the Spunk Addon](https://share.descript.com/view/KLl5lVdaOYZ)