---
description: This section is about how to configure the Splunk TA with IP Fabric.
---

# Splunk TA Overview

The IP Fabric Splunk TA offers a potent solution for comprehensive network
monitoring and analysis. By utilizing the official IP Fabric app, you can
seamlessly merge IP Fabric's advanced network visibility capabilities with
Splunk's robust data collection and analysis functionalities. This collaboration
empowers your teams to swiftly identify and troubleshoot network issues,
proactively enhance security measures, and gain valuable insights from the
amalgamation of IP Fabric's network data and Splunk's powerful analytics
platform.

## Installing the Add-On

### Splunkbase

The IP Fabric Splunk TA is available on Splunkbase:
<https://splunkbase.splunk.com/app/6707/>

![IP Fabric Splunkbase add-on](IPF_splunk_addon_splunkbase.png)

### Installing via the Splunk Web Interface

1. Log in to Splunk and navigate to the **Apps** menu.
2. Click the **Browse more apps** button or the **Find More Apps** link.
   ![Browse more apps or Find More Apps](IPF_splunk_manage_app.png)
3. Search for `IP Fabric`.
4. Click **Install**.
   ![Browse More Apps](IPF_splunk_addon_store.png)

### Manual Installation

1. Download the IP Fabric Splunk TA from
   <https://splunkbase.splunk.com/app/6707/>.
2. Log in to Splunk and navigate to the **Apps** menu.
3. Click **Manage Apps**.
4. Click **Install app from file**.
   ![Apps - Install app from file](IPF_splunk_local_app.png)
5. Select the IP Fabric Splunk TA file and click **Upload**.
   ![Upload app](IPF_splunk_local_upload.png)
6. Restart Splunk.

## Configuring the Add-On

### Setting Up IP Fabric API Token

1. Log in to IP Fabric and navigate to **Settings --> Integrations --> API
   Tokens**.
2. Click **+ Create token**.
3. Enter a description for the token, assign a role to it, and click **Create**.
4. Copy the token and save it in a secure location.
5. Log in to Splunk and navigate to the IP Fabric TA.
6. Click the **Configuration** tab.
7. Paste the token into the **API Token** field.
8. Click **Save**.

![IP Fabric API Token](IPF_splunk_addon_settings.png)

### Adding a Custom `strftime` Format

You can customize the timestamp format by adding a custom `strftime` format. Please reference the [Python `strftime` documentation](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes) for more information and how to set your own format code.

### Capturing Events

1. Log in to Splunk and navigate to the IP Fabric app.
2. Click the **Inputs** tab.
3. Click **Create New Input**.
   ![Inputs - Create New Input](IPF_splunk_configure_add_on.png)
4. Enter a **Name** for the input.
5. Enter an **Interval** for the input.
6. Enter an **Index** for the input.
7. Enter the **URL** of your IP Fabric instance.
   ![Update IP FABRIC form](IPF_splunk_capture_events.png)
8. Click **Update**.

Optional settings include:

- Sending the Snapshot Time with the event.
- Only capturing the amount of occurrences or rows in a specific table.
- Only capturing Intent Checks or Table Data.

## Using the Add-On

### Search an Index Loaded With IP Fabric Data

Using the search bar, you can search for any data that has been loaded into the
index. You can start correlating data in other Splunk indexes with IP Fabric
data.

![New Search](IPF_splunk_search_index.png)

<iframe src="https://share.descript.com/embed/KLl5lVdaOYZ" width="756" height="500" frameborder="0" allowfullscreen></iframe>

